extern crate phf;
#[macro_use]
extern crate rocket;
extern crate serde_derive;
extern crate serde_json;
extern crate urlencoding;

mod pons;
#[cfg(test)]
mod tests;

use async_trait::async_trait;
use pons::{PonsQueryFacade, PonsService};
use rocket::fs::FileServer;
use rocket::State;
use serde::Serialize;
use std::sync::Arc;

struct DictQuery {
    search_term: String,
    from: String,
    to: String,
}

async fn query_pons(
    ref_secret_provider: &Arc<pons::FileSecretProvider>,
    query: DictQuery,
) -> String {
    let query_facade = PonsQueryFacade {
        secret_provider: ref_secret_provider.clone(),
    };
    let pons_service = PonsService {
        query_facade: Arc::new(query_facade),
    };
    let pons_result = pons_service.lookup_word(query).await;
    serde_json::to_string(&pons_result).unwrap()
}

#[derive(Clone, Debug, PartialEq, Serialize)]
pub struct DictResult {
    source_term: String,
    target_term: String,
}

#[async_trait]
trait DictService {
    async fn lookup_word(&self, query: DictQuery) -> DictResult;
}

struct LinguaFrankaState {
    pons_secret_provider: Arc<pons::FileSecretProvider>,
}

#[get("/?<word>&<from>&<to>&<service>")]
async fn lookup(
    state: &State<LinguaFrankaState>,
    word: &str,
    from: &str,
    to: &str,
    service: &str,
) -> String {
    let decoded_word = urlencoding::decode(word).unwrap();
    let query = DictQuery {
        search_term: decoded_word.to_string(),
        from: from.to_string(),
        to: to.to_string(),
    };
    if service == "pons" {
        query_pons(&state.pons_secret_provider, query).await
    } else {
        format!(
            "You asked for '{}' from {} to {} using service {}",
            decoded_word, from, to, service
        )
    }
}

#[launch]
fn rocket() -> _ {
    let pons_file_secret_provider = pons::FileSecretProvider::new(pons::read_secret_from_file());
    rocket::build()
        .mount("/", FileServer::from("static/"))
        .mount("/lookup", routes![lookup])
        .manage(LinguaFrankaState {
            pons_secret_provider: Arc::new(pons_file_secret_provider),
        })
}
