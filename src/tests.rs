use super::pons::read_secret_from_file;
use super::pons::FileSecretProvider;
use super::rocket;
use super::LinguaFrankaState;
use rocket::local::blocking::Client;
use std::sync::Arc;

#[test]
fn pons_lookup() {
    let pons_file_secret_provider = FileSecretProvider::new(read_secret_from_file());
    let rocket = rocket::build()
        .mount("/lookup", routes![super::lookup])
        .manage(LinguaFrankaState {
            pons_secret_provider: Arc::new(pons_file_secret_provider),
        });
    let client = Client::tracked(rocket).unwrap();
    let word = urlencoding::encode("Verpflichtungserklärung");
    let response = client
        .get(format!(
            "/lookup?word={}&from=ger&to=fra&service=pons",
            word
        ))
        .dispatch();
    assert_eq!(
        response.into_string(),
        Some("déclaration d’engagement".into())
    );
}
