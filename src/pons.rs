use crate::{DictQuery, DictResult, DictService};
use async_trait::async_trait;
use mockall::predicate::*;
use mockall::*;
use phf::phf_map;
use restson::{Error, RestClient, RestPath};
use serde::Deserialize;
use std::ops::Deref;
use std::sync::{Arc, Mutex};

#[cfg(test)]
mod tests {
    use super::*;

    fn mock_dict_query() -> DictQuery {
        DictQuery {
            search_term: String::from("a"),
            from: String::from("ger"),
            to: String::from("fra"),
        }
    }

    fn mock_pons_query() -> PonsQuery {
        PonsQuery {
            search_term: String::from("a"),
            dictionary: String::from("defr"),
        }
    }

    fn mock_pons_result(_: PonsQuery) -> DictResult {
        DictResult {
            source_term: String::from("c"),
            target_term: String::from("d"),
        }
    }

    #[tokio::test]
    async fn test_query_data() {
        let mut mock = MockQueryFacade::new();
        mock.expect_send_query()
            .with(predicate::eq(mock_pons_query()))
            .times(1)
            .returning(mock_pons_result);
        let pons_service = PonsService {
            query_facade: Arc::new(mock),
        };
        let expected_result = mock_pons_result(mock_pons_query());
        let actual_result = pons_service.lookup_word(mock_dict_query()).await;
        assert_eq!(expected_result, actual_result);
    }
}

#[derive(Clone, Debug, PartialEq)]
pub struct PonsQuery {
    pub search_term: String,
    pub dictionary: String,
}

#[derive(Deserialize, Debug)]
struct Translation {
    source: String,
    target: String,
}

#[derive(Deserialize, Debug)]
struct Arabic {
    translations: Vec<Translation>,
}

#[derive(Deserialize, Debug)]
struct Roman {
    arabs: Vec<Arabic>,
}

#[derive(Deserialize, Debug)]
struct Hit {
    roms: Vec<Roman>,
}

#[derive(Deserialize, Debug)]
struct Item {
    hits: Vec<Hit>,
}

#[derive(Deserialize, Debug)]
struct PonsApi {
    items: Vec<Item>,
}

impl RestPath<PonsQuery> for PonsApi {
    fn get_path(query: PonsQuery) -> Result<String, Error> {
        let path = format!(
            "v1/dictionary?q={}&l={}",
            query.search_term, query.dictionary
        );
        println!("!!!path!!!: {}", path);
        Ok(path)
    }
}

#[automock]
#[async_trait]
pub trait QueryFacade {
    async fn send_query(&self, query: PonsQuery) -> DictResult;
}

pub trait SecretProvider {
    fn secret(&self) -> String;
}

pub struct FileSecretProvider {
    secret_string: Mutex<String>,
}

impl FileSecretProvider {
    pub fn new(secret_string: String) -> FileSecretProvider {
        FileSecretProvider {
            secret_string: Mutex::new(secret_string),
        }
    }
}

impl SecretProvider for FileSecretProvider {
    fn secret(&self) -> String {
        self.secret_string.lock().unwrap().deref().clone()
    }
}

pub fn read_secret_from_file() -> String {
    let file_secret = std::fs::read_to_string("pons/secret").unwrap();
    file_secret.trim().to_string()
}

pub struct PonsQueryFacade {
    pub secret_provider: Arc<dyn SecretProvider + Send + Sync + 'static>,
}

#[async_trait]
impl QueryFacade for PonsQueryFacade {
    async fn send_query(&self, query: PonsQuery) -> DictResult {
        let mut rest_client = RestClient::new("https://api.pons.com").unwrap();
        rest_client.set_header("X-Secret", &self.secret_provider.secret());
        let data = rest_client
            .get::<_, PonsApi>(query)
            .await
            .unwrap()
            .into_inner();
        let item = &data.items[0].hits[0].roms[0].arabs[0].translations[0];
        DictResult {
            source_term: String::from(item.source.clone()),
            target_term: String::from(item.target.clone()),
        }
    }
}

pub struct PonsService {
    pub query_facade: Arc<dyn QueryFacade + Send + Sync + 'static>,
}

static DICT_MAP: phf::Map<&'static str, &'static str> = phf_map! {
    "ger" => "de",
    "fra" => "fr",
};

#[async_trait]
impl DictService for PonsService {
    async fn lookup_word(&self, query: DictQuery) -> DictResult {
        let pons_from_opt = DICT_MAP.get(&query.from);
        let pons_to_opt = DICT_MAP.get(&query.to);
        if let Some(pons_from) = pons_from_opt {
            if let Some(pons_to) = pons_to_opt {
                let pons_query = PonsQuery {
                    search_term: query.search_term,
                    dictionary: format!("{}{}", pons_from, pons_to),
                };
                self.query_facade.send_query(pons_query).await
            } else {
                panic!("Wrong dictionary to key!");
            }
        } else {
            panic!("Wrong dictionary from key!");
        }
    }
}
